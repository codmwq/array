#include "functions.h"
#include "iostream"
using namespace std;
/**
 * @brief Function to find the minimum element
 * @param array
 * @param size
 * @return index
 */
int Index(int* array, int size)
{
    int check = 0;
    int index = 0;
    check = array[0];
    for (int i = 0;i < size; i++)
    {
        if(check > array[i])
        {
            check = array[i];
            index = i;
        }
    }
    return index;
}
/**
 * @brief Index of element
 * @param array
 * @param size
 * @param element
 * @return index
 */
int Element(int* array, int size,int element)
{
    int index = -1;
    for (int i = 0; i < size; i++)
    {
        if(array[i] == element)
        {
            index = i;
            cout << index << " ";
        }
    }
    return index;
}
/**
 * @brief Sort by ascending
 * @param array
 * @param size
 */
void SortU(int* array, int size)
{
    int sort = 0;
    for (int i = 1; i < size; i++)
    {
        for (int j = 0; j < size - 1; j++)
        {
            if (array[j] > array[j + 1])
            {
                sort = array[j];
                array[j] = array[j + 1];
                array[j + 1] = sort;
            }
        }
    }
}

/**
 * @brief Sort by descending
 * @param array
 * @param size
 */
void SortD(int* array, int size)
{
    int sort = 0;
    for (int i = 1; i < size; i++)
    {
        for (int j = 0; j < size - 1; j++)
        {
            if (array[j] < array[j + 1])
            {
                sort = array[j];
                array[j] = array[j + 1];
                array[j + 1] = sort;
            }
        }
    }
}
/**
 * @brief Function to find the longest sequence
 * @param array
 * @param size
 * @return compare
 */
int Sequence(int* array, int size)
{
    int count = 1;
    int compare = 1;
    for (int i = 0; i < size - 1; i++)
    {
        if(array[i] == array[i + 1])
        {
            count++;
            if (count > compare)
            {
                compare = count;
            }
        }
        else
        {
            count = 1;
        }
    }

    return compare;
}
/**
 * @brief Function to find the 3 max elements
 * @param array
 * @param max
 * @param size
 */
void Max(int* array,int* max, int size)
{
    int sort = 0;
    for (int i = 1; i < size; i++)
    {
        for (int j = 0; j < size - 1; j++)
        {
            if (array[j] < array[j + 1])
            {
                sort = array[j];
                array[j] = array[j + 1];
                array[j + 1] = sort;
            }
        }
    }
    for (int i = 0; i < 3 ;i++)
    {
        max[i] = array[i];
    }

}
/**
 * @brief Comparing of 2 arrays
 * @param array
 * @param array2
 * @param size
 * @param size2
 * @return compare
 */
bool CompareArray(int* array, int* array2, int size, int size2)
{
    bool compare = true;
    if(size == size2)
    {
        for (int i = 0; i < size; i++)
        {
            if(array[i] != array2[i])
            {
                compare = false;
                break;
            }
        }
    }
    else
    {
        compare = false;
    }
    return compare;
}


