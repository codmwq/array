#ifndef FUNCTIONS_H
#define FUNCTIONS_H
int Index(int* array, int size);
int Element(int* array, int size, int element);
void SortU(int* array, int size);
void SortD(int* array, int size);
int Sequence(int* array, int size);
void Max(int* array,int* max ,int size);
bool CompareArray(int* array, int* array2, int size, int size2);
#endif // FUNCTIONS_H
