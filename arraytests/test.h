#ifndef TEST_H
#define TEST_H
#include <QTest>
class test : public QObject
{
    Q_OBJECT

public:



private slots:
    void TestIndex();
    void TestElements();
    void TestSort();
    void TestSequence();
    void TestMax();
    void TestCompareArray();
};
#endif // TEST_H
